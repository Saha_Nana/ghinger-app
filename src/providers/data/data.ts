import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class DataProvider {

 
  constructor(public http: Http) {
    console.log('Hello DATAProvider Provider');
  }

   hosp_url = 'http://5.153.40.138:6099/hospital';
   signup_url = 'http://5.153.40.138:6099/sign_up';
   update_regis_url = 'http://5.153.40.138:6099/update_registration';
   login_url = 'http://5.153.40.138:6099/login';
   retrieve_url = 'http://5.153.40.138:6099/retrieve_regis';
   retrieve_url1 = 'http://5.153.40.138:6099/retrieve_regis1';
   retrieve_pers_url = 'http://5.153.40.138:6099/retrieve_doc';
   retrieve_doc_url = 'http://5.153.40.138:6099/retrieve_doc2';
   appoint_url = 'http://5.153.40.138:6099/make_appointment';
   doc_appoint_url = 'http://5.153.40.138:6099/doc_request';
    phone_consult_url = 'http://5.153.40.138:6099/phone_consult';
     prescription_url = 'http://5.153.40.138:6099/prescription';
    retrieve_phone_cons_url = 'http://5.153.40.138:6099/doctor_patient';
    retrieve_video_cons_url = 'http://5.153.40.138:6099/doctor_patient2';
    retrieve_home_cons_url = 'http://5.153.40.138:6099/doctor_patient3';
    retrieve_accepts_url = 'http://5.153.40.138:6099/doctor_patient4';
    prescription_list_url ='http://5.153.40.138:6099/prescription_list'
    l_appoint_url = 'http://5.153.40.138:6099/lab_appointment';
    medication_url = 'http://5.153.40.138:6099/medication';
    appoint_history_url = 'http://5.153.40.138:6099/appointment_history';
    personal_doc_appoint_history_url = 'http://5.153.40.138:6099/personal_doc_appointment_history';
    lab_service_url = 'http://5.153.40.138:6099/lab_services';
    investigation_url =  'http://5.153.40.138:6099/order_investigation';
    retrieve_investigation_url = 'http://5.153.40.138:6099/retrieve_order_investigation';

hospitals(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.hosp_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

registration(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.signup_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

update_registration(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.update_regis_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

ghinger_login(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.login_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve1(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_url1, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


retrieve_pers(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_pers_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve_doc(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_doc_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


appointment(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.appoint_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

doc_appointment(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.doc_appoint_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

phone_consult(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.phone_consult_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

prescription_list(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.prescription_list_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}



prescription(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.prescription_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

// doc_appointment(data) {
//   return new Promise((resolve, reject) => {
//     this.http.post(this.doc_appoint_url, JSON.stringify(data))
//       .subscribe(res => {
//         resolve(res);
//       }, (err) => {
//         reject(err);
//       });
//   });
// }


l_appointment(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.l_appoint_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}



medication(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.medication_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


investigation(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.investigation_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


retrieve_investigation(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_investigation_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}



appointment_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.appoint_history_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

personal_doc_appointment_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.personal_doc_appoint_history_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


retrieve_phone_consult(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_phone_cons_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve_video_consult(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_video_cons_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve_home_consult(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_home_cons_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve_accepted_appointment(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_accepts_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}



get_lab_services() {
  return new Promise((resolve, reject) => {
    this.http.get(this.lab_service_url)
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}
 

}
