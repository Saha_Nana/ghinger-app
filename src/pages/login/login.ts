import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { MenuPage } from '../menu/menu';
import { NoticePage } from '../notice/notice';
import { PasswordPage } from '../password/password';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { DoctorPage } from '../doctor/doctor';
import { FormBuilder, Validators } from '@angular/forms';





@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = true;

  public loginForm: any;
  submitAttempt: boolean = false;
  loginVal: any;
  jsonBody: any;
  jsonBody1: any;

  messageList: any;
  api_code: any;
  Phonenumber: string;
  PIN: string;
  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;

  searchbar: any;
  from_login: any = [];

  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  body1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doc_id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];

  // params: any = [];

  requester_id: any;
  data1: any = [];

  constructor(public toastCtrl: ToastController, public data: DataProvider, public _form: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http) {

    this.loginForm = this._form.group({

      "password": ["", Validators.compose([Validators.required])],
      "username": ["", Validators.compose([Validators.required])]



    })

  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
    console.log('ionViewDidLoad SliderPage');
  }



  params = {

    "password": "",
    "username": ""
  }




  signup() {

    this.navCtrl.push(SignupPage)


  }

  login() {

    this.loginVal = JSON.stringify(this.loginForm.value);
    console.log("LETS SEE THE LOGIN VAL " + this.loginVal)

    this.jsonBody = JSON.parse(this.loginVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE SIGNUP VALUES STRINGIFY" + JSON.stringify(this.jsonBody))



    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();



    this.data.retrieve1(this.jsonBody).then((result) => {


      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.retrieve1 = body1
      this.retrieve1 = JSON.stringify(body1)

      console.log('LETS SEE THE  BODY ' + body1);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve1);

      this.body1 = Array.of(this.retrieve1)
      this.jsonBody1 = JSON.parse(this.body1);
      this.person_type1 = this.jsonBody1[0].user_type
      this.doctor_id2 = this.jsonBody1[0].id

      console.log('-----------------------------------------------------');
      console.log('VALUE of USER TYPE  IS ' + this.person_type1);
      console.log('VALUE of REGIS ID  IS ' + this.doctor_id2);
      console.log('-----------------------------------------------------');

    });



    this.data.retrieve(this.jsonBody).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);

      this.retrieve = body
      this.retrieve = JSON.stringify(body)

      console.log('-----------------------------------------------------');
      console.log('-----------------THIS IS A LOG IN RETRIEVAL------------------------------------');
      console.log('LETS SEE THE  BODY ' + body);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
      console.log('-----------------------------------------------------');

      this.body = Array.of(this.retrieve)
      this.jsonBody = JSON.parse(this.body);
      this.person_type = this.jsonBody[0].person_type


      console.log('VALUE of PERSON TYPE  IS ' + this.person_type);

    });

    this.data.retrieve_pers(this.jsonBody).then((result) => {


      var body = result["_body"];
      body = JSON.parse(body);

      this.retrieve_pers = body
      this.retrieve_pers = JSON.stringify(body)

      console.log('LETS SEE THE  BODY ' + body);
      console.log('LETS SEE THE PERSON INFO DATA RETRIEVED ' + this.retrieve_pers);

      this.body = Array.of(this.retrieve_pers)
      this.jsonBody = JSON.parse(this.body);
      this.doctor_id = this.jsonBody[0].doctor_id


      console.log('VALUE of DOCTOR ID IS ' + this.doctor_id);

      this.doc_id = JSON.stringify(this.doctor_id);
      this.jsonBody1 = JSON.parse(this.doc_id);
      console.log("THIS IS THE JSON VALUES " + this.jsonBody1)


      this.doc_params = {

        "id": this.doctor_id2

      }

        this.doc_params2 = {

        "id": this.doctor_id

      }
      console.log("------------DOC PARAMS----------");
      console.log(this.doc_params);


      this.data.retrieve_doc(this.doc_params).then((result) => {

        var body = result["_body"];
        body = JSON.parse(body);

        this.retrieve_doc = body
        this.retrieve_doc = JSON.stringify(body)
        console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.retrieve_doc);


      });



       this.data.retrieve_doc(this.doc_params2).then((result) => {

        var body = result["_body"];
        console.log("LETS SEE BODY " + body)
        if (body == ""){
        console.log("BODY IS EMPTY" )
      }
      
        else{ 
        body = JSON.parse(body);

        this.retrieve_doc3 = body
        this.retrieve_doc3 = JSON.stringify(body)
        console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.retrieve_doc3);
        }

      });



    });



    this.data.ghinger_login(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      loader.dismiss();
      if (this.api_code == "117") {
        let loader = this.loadingCtrl.create({
          content: "Login processing..."
        });

        loader.present();


        if (this.person_type1 == "D") {

          setTimeout(() => {
            this.navCtrl.setRoot(DoctorPage,

              { value: this.retrieve, doc_value: this.retrieve_doc, pers_value: this.retrieve_pers ,doc_value2: this.retrieve1 });

          }, 3000);

          setTimeout(() => {
            loader.dismiss();
          }, 3000);
        }


        else {
          setTimeout(() => {
            this.navCtrl.setRoot(MenuPage,

              { value: this.retrieve, doc_value: this.retrieve_doc3, pers_value: this.retrieve_pers });

          }, 3000);

          setTimeout(() => {
            loader.dismiss();
          }, 3000);

        }

      }

      if (this.api_code == "201") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "118") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "103") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }


      if (this.api_code == "222") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "001") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }



    },

      (err) => {

        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "No internet connection",
          buttons: ['OK']
        });
        alert.present();

        this.toastCtrl.create({
          message: "Please check your internet connection",
          duration: 5000
        }).present();
        loader.dismiss();
        console.log(err);
      });
  }


  reset(){

this.navCtrl.push(PasswordPage)

  }


}

