import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { ToastController,LoadingController,AlertController , ModalController  } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { HospitalListPage } from '../hospital-list/hospital-list';
import { HospitalList1Page } from '../hospital-list1/hospital-list1';
import { BookHomePage } from '../../pages/book-home/book-home';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
import {Http} from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';



@Component({
  selector: 'page-home-search',
  templateUrl: 'home-search.html',
})
export class HomeSearchPage {
 @ViewChild('searchbar')myInput;
 @ViewChild('input') 
  searchbar: any;

messageList: any;
api_code: any;
location: any;
displayData: any;
check: any;
from_menu: any = [];
body: any;
jsonBody: any;
params: any = [];
from_login: any = [];
from_login2: any = [];
from_login3: any = [];
sub_id: any;
  string: any;

  constructor(private keyboard: Keyboard,public navCtrl: NavController,public navParams: NavParams,public completeTestService: CompleteTestService,public data: DataProvider,public loadingCtrl:LoadingController,public alertCtrl: AlertController,public modalCtrl: ModalController,public viewCtrl: ViewController) {
 this.from_login =  this.navParams.get('value')
    this.from_login2 = this.navParams.get('pers_value')
      this.from_login3 = this.navParams.get('doc_value')
    console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);


    // setTimeout(() => {
    //   this.keyboard.show(); // for android
    //   this.myInput.setFocus();
    // },1000);

    // this.searchbar.setFocus() 
 


 }

  ionViewDidLoad() {

    // setTimeout(() => {
    //   this.keyboard.show(); // for android
    //   this.myInput.setFocus();
    // },1000);

   

 }


 ionViewDidEnter(){

    // setTimeout(() => {
    //    this.keyboard.show(); // for android
    //   this.myInput.setFocus();
    // },1000);



 }


closeModal(){


  this.viewCtrl.dismiss();
}





  go(){




    let loader = this.loadingCtrl.create({
        content: "Please wait ..."
  
      });

      loader.present(); 

    let data = this.searchbar.getValue()
      console.log("LOCATION ENTERED " + data)

    if (data == undefined ){

       let alert = this.alertCtrl.create({
          title: "",
          subTitle: "Please enter your location.Your location should be on ghinger.",
          buttons: ['OK']
        });
        alert.present();
        loader.dismiss();
     }


     else{

     this.params = {

      "location": data

    }
    console.log('PARAMETERS' + this.params);

    this.data.hospitals(this.params).then((result) => {

      console.log("RESULTS IS " + result);
      console.log("RESULTS IS" + this.data.hospitals(this.params));
      var body = result["_body"];
      body = JSON.parse(body);
      this.check = body
      console.log("RESULTS IS " + this.check);
      this.string=  JSON.stringify(this.check)
     console.log("LETS SEE THE STRING " + this.string)

      this.jsonBody = JSON.parse(this.string);

     this.sub_id = this.jsonBody[0].suburb_id
     console.log("LETS SEE THE Surburb " + this.sub_id)
   


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

    }, (err) => {


      console.log(err);
    });


    
    console.log("VALUES FROM LOCATION SEARCH" + data);
    console.log(data);
setTimeout(() => {
    //  this.navCtrl.push(HospitalList1Page,{value: data, another: this.from_login},);
 this.navCtrl.push(BookHomePage,{value: data, another: this.from_login,sub_id: this.sub_id, doc_value: this.from_login3,pers_value: this.from_login2},);

  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

    }
  }


}
