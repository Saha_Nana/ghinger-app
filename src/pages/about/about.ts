import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController,AlertController   } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,public alertCtrl: AlertController) {

    //  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Logout?',
      message: 'Do you agree to logout ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
             let loader = this.loadingCtrl.create({
        content: "Logging out...",
        duration: 5000
       
      
      });

            loader.present();
     
       setTimeout(() => {

    this.navCtrl.setRoot(LoginPage);
  
  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

          }
        }
      ]
    });
    confirm.present();
  }

  }

// }
