import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';



@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {


  public signupForm: any;
  submitAttempt: boolean = false;
  messageList: any;
  api_code: any;
  signupVal: any;
  jsonBody: any;

  public itemList: Array<Object>;

  constructor(public navCtrl: NavController, public data: DataProvider, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    this.signupForm = this._form.group({
      "surname": ["", Validators.compose([Validators.required])],

      "other_names": ["", Validators.compose([Validators.required])],
      "telco": ["", Validators.compose([Validators.required])],
      "mobile_number": ["", Validators.compose([Validators.required])],
      "email": ["",Validators.compose([Validators.required])],
      "user_type": ["", Validators.compose([Validators.required])],
      "username": ["", Validators.compose([Validators.required])],
      "password": ["", Validators.compose([Validators.required])],
      "dob": ["", Validators.compose([Validators.required])],


    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  signup() {

    this.signupVal = JSON.stringify(this.signupForm.value);

    this.jsonBody = JSON.parse(this.signupVal);

    console.log("THIS IS THE SIGNUP raw values VALUES" + this.signupVal)
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)



    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    });

    loader.present();

    this.data.registration(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];

      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Signing up..."
          // duration: 5000


        });
        loader.present();


        setTimeout(() => {
          
          let alert = this.alertCtrl.create({
          title: '',
          subTitle: "Sign up has been successful. Kindly login..",
          buttons: ['OK']
        });
        alert.present();
          
          this.navCtrl.setRoot(LoginPage, { value: this.jsonBody });
        }, 3000);
        

        setTimeout(() => {
              let alert = this.alertCtrl.create({
          title: '',
          subTitle: "Sign up has been successful. Kindly login..",
          buttons: ['OK']
        });
          loader.dismiss();
        }, 3000);
        

         this.toastCtrl.create({
          message: "Sign up has been successful. Kindly login..",
          duration: 5000
        }).present();


        

      }

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }
    }, (err) => {

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "No internet connection",
        buttons: ['OK']
      });
      alert.present();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      loader.dismiss();
      console.log(err);
    });
  }



  signin() {

    this.navCtrl.push(LoginPage)
  }



}
