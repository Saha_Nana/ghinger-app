import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { PhoneConsultPage } from '../phone-consult/phone-consult';
import { VideoConsultPage } from '../video-consult/video-consult';
import { PersonalHomeCarePage } from '../personal-home-care/personal-home-care';
import { PrescriptionPage } from '../prescription/prescription';

@Component({
  selector: 'page-personal-wel',
  templateUrl: 'personal-wel.html',
})
export class PersonalWelPage {
   from_login: any = [];
     from_login_doc: any = [];
     from_login_pers: any = [];
  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  surname: any;
  first_name: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];

  requester_id: any;
  data1: any = [];

  constructor(public alertCtrl: AlertController,public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
 this.from_login = this.navParams.get('value')
 this.from_login_doc = this.navParams.get('doc_value')
  this.from_login_pers = this.navParams.get('pers_value')
    console.log('VALUE IN PERSONAL WELL CONSTRUCTOR IS' + this.from_login); 
     console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login_doc); 


     this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login_doc)

    this.jsonBody = JSON.parse(this.body);

    this.surname = this.jsonBody[0].surname
    this.first_name = this.jsonBody[0].other_names

    console.log("DOC NAME " + this.surname +  this.first_name)
    

 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PersonalWelPage');
  }



  phone(){

     this.navCtrl.push(PhoneConsultPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

//  let modal = this.modalCtrl.create(PhoneConsultPage, {

//       value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers

//     });

//     modal.present();


  }


 video(){
    this.navCtrl.push(VideoConsultPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

//  let modal = this.modalCtrl.create(VideoConsultPage, {

//       value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers

//     });

//     modal.present();


  }

  homecare(){

    this.navCtrl.push(PersonalHomeCarePage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
    //  let modal = this.modalCtrl.create(PersonalHomeCarePage, {

    //   value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers

    // });

    // modal.present();


  }


prescription(){

    // let modal = this.modalCtrl.create(PrescriptionPage, {

    //   value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers

    // });

    // modal.present();

     this.navCtrl.push(PrescriptionPage, { value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });


  }

}