import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PatientNewRecordPage } from '../patient-new-record/patient-new-record';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { RecordPage } from '../record/record';
import { RemovehtmltagsPipe } from '../../pipes/removehtmltags/removehtmltags';


import { Http } from '@angular/http';
import 'rxjs/add/operator/map';



@Component({
  selector: 'page-patient-details',
  templateUrl: 'patient-details.html',
  //pipes: [RemovehtmltagsPipe]
})
export class PatientDetailsPage {
  from_login: any = [];
  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  doctor_id: any;
  requester_id: any;
  // data: any = [];
  data1: any;
  data2: any;
  body2: any;
  dob: any;
  phm: any;
  doc_details: any;
  retrieve:any;
  records: any;
  date: any;

  constructor(public modalCtrl: ModalController,public data: DataProvider, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.check = this.navParams.get('value')
    this.data2 = this.navParams.get('user_data')
    this.doc_details = this.navParams.get('doc_details')
    this.retrieve = this.navParams.get('retrieve')
    console.log('VALUE IN PATIENT DETAIL CONSTRUCTOR IS' + this.check);
    console.log('VALUE IN PATIENT DETAIL CONSTRUCTOR STRINGIFIED IS' + JSON.stringify(this.check));
    console.log("Value of User data in patient details page is " + this.data2)
    console.log("Value of User data in patient details page is STRINGIFIED" + JSON.stringify(this.data2))
    console.log("Value of DOC details in patient details page is " + this.doc_details)
    console.log("Value of doc_details in patient details page is STRINGIFIED" + JSON.stringify(this.doc_details))
     console.log("Value of retieve order inves in patient details page" + this.retrieve)
      console.log("Value of retieve order inves parse in patient details page" + JSON.parse(this.retrieve))

      this.records = JSON.parse(this.retrieve)
      // this.date = this.records[0].created_at
      // console.log ("LETS SEE THE DATE " +  this.date)
      // let dateFormat = this.date.split('T');
      // console.log ("FORMATTED  DATE " +  dateFormat)

    this.body = JSON.stringify(this.data2)
    this.body2 = Array.of(this.data2)
    console.log("ARRAY " + this.body2)
    // this.jsonBody = JSON.parse(this.body2);
    console.log("JSON " + this.body)
    this.requester_id = this.body2[0].patient_id
    this.dob = this.body2[0].dob


    console.log("THIS IS THE requester_id " + this.requester_id)
    console.log("THIS IS DATE OF BIRTH " + this.dob)

    this.params = {

      "requester_id": this.requester_id


    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    this.data.personal_doc_appointment_history(this.params).then((result) => {

      console.log("RESULTS IS " + result);
      var body = result["_body"];
      body = JSON.parse(body);
      console.log("LESTS SEE BODY " + body);
      this.data1 = body
      console.log("DATA1 RESULTS IS " + this.data1);
      console.log("STRINGIFY DATA1 RESULTS IS " + JSON.stringify(this.data1));
      this.body = Array.of(this.data1)
      this.phm = this.data1[0].prev_medical_history.trim()
      this.location = this.data1[0].suburb_name

      console.log("PREVIOUS MED HISTORY " + this.data1[0].prev_medical_history.trim());

     // loader.dismiss();
    });




     this.params = {
      "patient_id": this.requester_id
      }

    //   let loader = this.loadingCtrl.create({
    //   content: "Please wait ..."

    // });

    // loader.present();

    this.data.retrieve_investigation(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);

      this.retrieve = body
      this.retrieve = JSON.stringify(body)

      console.log('-----------------------------------------------------');
      console.log('-----------------THIS IS A LOG IN RETRIEVAL------------------------------------');
      console.log('LETS SEE THE  BODY ' + body);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
      console.log('-----------------------------------------------------');

      this.records = JSON.parse(this.retrieve)
      this.body = Array.of(this.retrieve)
      this.jsonBody = JSON.parse(this.body);


       var desc = body["resp_desc"];
      var code = body["resp_code"];

 console.log('-----------------RESP CODE------------------------------------');
      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;
 console.log('-----------------------------------------------------');
      loader.dismiss();

    });


  }



  create() {
    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();
    loader.dismiss();
    this.navCtrl.push(PatientNewRecordPage, { 'value': this.check, "user_data": this.data2 , 'doc_details': this.doc_details ,'retrieve': this.retrieve})
  }


  open(item){

    console.log("LEts see item in open fucntion " + JSON.stringify(item)) 

      let modal = this.modalCtrl.create(RecordPage, {

        'value': this.check, "user_data": this.data2 , 'doc_details': this.doc_details ,'retrieve': this.retrieve, 'item_list': item}

      );

      modal.present();
    }


}
