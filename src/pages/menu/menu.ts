import { Component, ViewChild } from '@angular/core';
import { MenuController, NavController, NavParams,App } from 'ionic-angular';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
import { MedAppointPage } from '../med-appoint/med-appoint';
import { PersonalWelPage } from '../personal-wel/personal-wel';
import { VidConsultPage } from '../vid-consult/vid-consult';
import { LabHistoryPage } from '../lab-history/lab-history';
import { MedicationSearchPage } from '../medication-search/medication-search';
import { HomeSearchPage } from '../home-search/home-search';
import { DocHistoryPage } from '../doc-history/doc-history';
import { LocationPage } from '../location/location';
import { LoginPage } from '../login/login';
import { SearchPage } from '../search/search';
import { LabSearchPage } from '../lab-search/lab-search';
import { DataProvider } from '../../providers/data/data';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';


const NOT_PERMITTED = "Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!"



@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',

})
export class MenuPage {

  @ViewChild('searchbar')

  searchbar: any;
  from_login: any = [];
  from_login_doc: any = [];
  from_login_pers: any = [];
  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  parsed_name: any;
  requester_id: any;
  doc_id: any;
  doc_id1: any;
  data1: any = [];
  username: any;


  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;
  body1: any;
  jsonBody1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];

  constructor(public app: App,public menuCtrl: MenuController, public data: DataProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public loadingCtrl: LoadingController) {
    

   
    this.menuCtrl.enable(true);
    this.from_login = this.navParams.get('value')
    this.from_login_doc = this.navParams.get('doc_value')
    this.from_login_pers = this.navParams.get('pers_value')
    console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS' + this.from_login);
    console.log('LOGIN DETAILS from LOGIN DOC IN MENU PAGE FOR CONSTRUCTOR IS' + this.from_login_doc);
    console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS' + this.from_login_pers);


    this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
    this.requester_id = this.jsonBody[0].id
    this.username = this.jsonBody[0].username
    this.check = this.jsonBody[0]

    this.body = Array.of(this.from_login_pers)
    this.jsonBody = JSON.parse(this.body);
    this.doc_id = this.jsonBody[0].doctor_id
    this.check = this.jsonBody[0]

    console.log("USERNAME HERE IS " + this.username)
    console.log('VALUE of requester IN Menu  IS ' + this.requester_id);
    console.log('VALUE of DOCTOR ID IN Menu  IS ' + this.doc_id);

  }

//    openMenu() {
//    this.menuCtrl.open();
//       this.menuCtrl.toggle();
//       this.menuCtrl.enable(true, "mymenu");
//        console.log("PRESSSIN MENU")
//  }

 openMenu(){



    if(this.menuCtrl.isOpen()){
        console.log("is open");
    }
    if(this.menuCtrl.isEnabled()){
        console.log("is enabled");
    }

    this.menuCtrl.toggle();
    this.menuCtrl.open();

 }


// ionViewDidLeave(){
//     this.navCtrl.pop();
// }


  ionViewDidLoad() {
    this.menuCtrl.enable(true);
  }

  ionViewDidEnter(){
     this.menuCtrl.enable(true);


  }

  ionViewWillEnter(){
     this.menuCtrl.enable(true);
  }




  medical() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {

       this.navCtrl.push(SearchPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

      // let modal = this.modalCtrl.create(SearchPage, {

      //   value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
      // });
      // modal.present();
    }
  }

   medication() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {

 this.navCtrl.push(MedicationSearchPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
      // let modal = this.modalCtrl.create(MedicationSearchPage, {

      //   value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
      // });
      // modal.present();

    }
  }



  // this.navCtrl.push(MedAppointPage, { value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });




  lab_history() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
 this.navCtrl.push(LabSearchPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

    //   let modal = this.modalCtrl.create(LabSearchPage, {

    //     value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers
    //   });
    //   modal.present();
     }
  }
  // this.navCtrl.push(LabHistoryPage, { value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers});

  //}



  doc_history() {
    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    if (this.doc_id != null) {

      this.navCtrl.push(PersonalWelPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
    }

    if (this.requester_id != null && this.doc_id == null) {

      // this.navCtrl.push(LocationPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

      let modal = this.modalCtrl.create(LocationPage, {

        value: this.from_login, pers_value: this.from_login_pers, doc_value: this.from_login_doc

      });

      modal.present();
    }
  }




  video() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {

      let modal = this.modalCtrl.create(VidConsultPage, {

        value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers

      });

      modal.present();
    }

  }


  home() {

    if (this.requester_id == null) {

      let alert = this.alertCtrl.create({
        title: '',
        subTitle: NOT_PERMITTED,
        buttons: ['OK']
      });
      alert.present();
    }

    else {
      this.navCtrl.push(HomeSearchPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

    }


  }







  refresh() {
    this.from_login = this.navParams.get('value')
     this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
    this.requester_id = this.jsonBody[0].id
    this.username = this.jsonBody[0].username
    this.check = this.jsonBody[0]

    this.body = Array.of(this.from_login_pers)
    this.jsonBody = JSON.parse(this.body);
    this.doc_id = this.jsonBody[0].doctor_id
    this.check = this.jsonBody[0]

    console.log("USERNAME HERE IS " + this.username)
    console.log('VALUE of requester IN Menu  IS ' + this.requester_id);
    console.log('VALUE of DOCTOR ID IN Menu  IS ' + this.doc_id);

    console.log("THIS IS THE USERNAME " + this.username)
    //  console.log ("THIS IS THE JSON PARSED USERNAME " + this.parsed_name)


    this.params = {

      "username": this.username

    }

    let loader = this.loadingCtrl.create({
      content: "Refreshing ...",
    });

    loader.present();



    this.data.retrieve1(this.params).then((result) => {


      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.retrieve1 = body1
      this.retrieve1 = JSON.stringify(body1)

      console.log('LETS SEE THE  BODY ' + body1);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve1);

      this.body1 = Array.of(this.retrieve1)
      this.jsonBody1 = JSON.parse(this.body1);
      this.person_type1 = this.jsonBody1[0].user_type
      this.doctor_id2 = this.jsonBody1[0].id

      console.log('-----------------------------------------------------');
      console.log('VALUE of USER TYPE  IS ' + this.person_type1);
      console.log('VALUE of REGIS ID  IS ' + this.doctor_id2);
      console.log('-----------------------------------------------------');

    });



    this.data.retrieve(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);

      this.retrieve = body
      this.retrieve = JSON.stringify(body)

      console.log('-----------------------------------------------------');
      console.log('-----------------THIS IS A LOG IN RETRIEVAL------------------------------------');
      console.log('LETS SEE THE  BODY ' + body);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
      console.log('-----------------------------------------------------');

      this.body = Array.of(this.retrieve)
      this.jsonBody = JSON.parse(this.body);
      this.person_type = this.jsonBody[0].person_type_id
      this.requester_id = this.jsonBody[0].id



      console.log('VALUE of PERSON TYPE  IS ' + this.person_type);
      console.log('VALUE of requester IN Menu  IS ' + this.requester_id);

    });

    this.data.retrieve_pers(this.params).then((result) => {


      var body = result["_body"];
      body = JSON.parse(body);

      this.from_login_pers = body
      this.from_login_pers = JSON.stringify(body)

      console.log('LETS SEE THE  BODY ' + body);
      console.log('LETS SEE THE PERSON INFO DATA RETRIEVED ' + this.from_login_pers);

      this.body = Array.of(this.from_login_pers)
      this.jsonBody = JSON.parse(this.body);
      this.doc_id = this.jsonBody[0].doctor_id


      console.log('VALUE of DOCTOR ID IS ' + this.doc_id);

      this.doc_id1 = JSON.stringify(this.doc_id);
      this.jsonBody1 = JSON.parse(this.doc_id1);
      console.log("THIS IS THE JSON VALUES " + this.jsonBody1)


      this.doc_params = {

        "id": this.doctor_id2

      }

      this.doc_params2 = {

        "id": this.doc_id

      }
      console.log("------------DOC PARAMS----------");
      console.log(this.doc_params);


      this.data.retrieve_doc(this.doc_params).then((result) => {

        var body = result["_body"];
        body = JSON.parse(body);

        this.retrieve_doc = body
        this.retrieve_doc = JSON.stringify(body)
        console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.retrieve_doc);


      });



      this.data.retrieve_doc(this.doc_params2).then((result) => {

        var body = result["_body"];
        console.log("LETS SEE BODY " + body)
        if (body == "") {
          console.log("BODY IS EMPTY")
        }

        else {
          body = JSON.parse(body);

          this.from_login_doc = body
          this.from_login_doc = JSON.stringify(body)
          console.log('LETS SEE THE DOCTOR INFO DATA RETRIEVED ' + this.from_login_doc);
        }

      });



    });
    loader.dismiss();


  }


}
