import { Component, ViewChild } from '@angular/core';
import { MenuController, NavController, NavParams, ViewController, App, Platform } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';
import { MenuPage } from '../menu/menu';
import { getRootNav } from '../../utils/navUtils';
import { NgZone } from '@angular/core';


@Component({
  selector: 'page-vid-consult',
  templateUrl: 'vid-consult.html',
})
export class VidConsultPage {


  alph: any;
  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  params2: any = [];
  from_login: any = [];
  from_login_doc: any = [];
  from_login_pers: any = [];
  sub_id: any;
  string: any;
  app_date: any;
  app_time: any;
  requester_id: any;

  constructor(private zone: NgZone,public platform: Platform, public app: App, public menu: MenuController, public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

    this.from_login = this.navParams.get('value')
    this.from_login_doc = this.navParams.get('doc_value')
    this.from_login_pers = this.navParams.get('pers_value')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneConsultPage');
  }


  closeModal() {
    this.viewCtrl.dismiss();
  }



  submit() {

    this.from_login = this.navParams.get('value')
    console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);
    this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
    this.requester_id = this.jsonBody[0].id
    this.check = this.jsonBody[0]

    console.log("THIS IS THE REquester ID " + this.requester_id)
    console.log("THIS IS THE APP DATE " + this.app_date)
    console.log("THIS IS THE APP TIME " + this.app_time)

    this.params2 = {

      "appointment_type_id": "VC",
      "requester_id": this.requester_id,
      "proposed_date": this.app_date + " " + this.app_time, 
     
    }

    console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2))

    console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2))


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    this.data.phone_consult(this.params2).then((result) => {

      console.log("THIS IS THE RESULT" + result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)


      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: "Video Consult Appointment made Succesfully. You would be contacted shortly. Thank you!",
          buttons: ['OK']
        });

        //const root = this.app.getRootNav();
       // root.popToRoot();
       // root.push(MenuPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });


    //     this.zone.run(() => {
    // this.navCtrl.setRoot(MenuPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
    //     });
       
        this.navCtrl.pop()
       // this.navCtrl.popToRoot(MenuPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
        // this.navCtrl.setRoot(MenuPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });

        //this.viewCtrl.dismiss();
        //this.alph = this.app.getRootNav()

        // this.alph =  this.app.getRootNavById('n4')
        //this.alph.setRoot(MenuPage, { value: this.from_login, doc_value: this.from_login_doc, pers_value: this.from_login_pers });
        //this.app.getRootNav().setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });
        //this.navCtrl.setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });
        //this.navCtrl.popToRoot() 
        //     this.navCtrl.setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers }).then(() =>{
        //     this.navCtrl.popToRoot();
        //     }).catch(err=>{
        //  console.log(err.toString());
        // });

        // this.platform.ready().then(() => {
        //     this.navCtrl.setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers })
        // });

        //     const root = this.app.getRootNav();
        // root.popToRoot();
        // root.setRoot("LoginPage"); 

        // this.menu.close();
        // this.nav.setRoot(HomePage).then(()=>{
        // this.nav.popToRoot();
        // }).catch(err=>{
        // alert(err.toString());
        // });
        //....
        //});
        alert.present();

      }


      if (this.api_code == "555") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });


        alert.present();
      }


    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });

  }




}
