import { Component, ViewChild } from '@angular/core';
import { MenuController,NavController, NavParams, ViewController ,App} from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-refer',
  templateUrl: 'refer.html',
})
export class ReferPage {

 constructor(public _form: FormBuilder,public app: App,public menu: MenuController,public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController,public modalCtrl: ModalController, public viewCtrl: ViewController) {

  //  this.appointForm = this._form.group({

  //     "requester_cat": ["", Validators.compose([Validators.required])],
  //     "lab_services":[""],
  //     "beneficiary_name": [""],
  //     "req_urgency": ["", Validators.compose([Validators.required])],
  //     "proposed_date": [""],
  //     "proposed_time": [""],
  //     "appointment_type_id": ["LA"],
  //     "test_list": ["", Validators.compose([Validators.required])],
  //     "prev_medical_history": ["", Validators.compose([Validators.required])],
  //     "allergies": [""]


  //   })  

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


  //  submit() {

  //   this.from_login = this.navParams.get('value')
  //   console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);
  //   this.body = Array.of(this.from_login)
  //   this.jsonBody = JSON.parse(this.body);
  //   this.requester_id = this.jsonBody[0].id
  //   this.check = this.jsonBody[0]

  //   console.log("THIS IS THE REquester ID " + this.requester_id)
  //   console.log("THIS IS THE APP DATE " + this.app_date)
  //   console.log("THIS IS THE APP TIME " + this.app_time)

  //   this.params2 = {

  //     "appointment_type_id": "VC",
  //     "requester_id": this.requester_id,
  //     "proposed_date": this.app_date + " " + this.app_time, 
     
  //   }

  //   console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2))

  //   console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2))


  //   let loader = this.loadingCtrl.create({
  //     content: "Please wait ..."

  //   });

  //   loader.present();

  //   this.data.phone_consult(this.params2).then((result) => {

  //     console.log("THIS IS THE RESULT" + result);
  //     var jsonBody = result["_body"];
  //     console.log(jsonBody);

  //     jsonBody = JSON.parse(jsonBody);
  //     console.log(jsonBody)


  //     var desc = jsonBody["resp_desc"];
  //     var code = jsonBody["resp_code"];


  //     console.log(desc);
  //     console.log(code);

  //     this.messageList = desc;
  //     this.api_code = code;

  //     loader.dismiss();

  //     if (this.api_code == "000") {
  //       let alert = this.alertCtrl.create({
  //         title: '',
  //         subTitle: "Video Consult Appointment made Succesfully. You would be contacted shortly. Thank you!",
  //         buttons: ['OK']
  //       });

      
  //       this.navCtrl.pop()
      
  //       alert.present();

  //     }


  //     if (this.api_code == "555") {
  //       let alert = this.alertCtrl.create({
  //         title: '',
  //         subTitle: this.messageList,
  //         buttons: ['OK']
  //       });


  //       alert.present();
  //     }


  //   }, (err) => {
  //     loader.dismiss();
  //     this.toastCtrl.create({
  //       message: "Please check your internet connection",
  //       duration: 5000
  //     }).present();

  //     console.log(err);
  //   });

  // }




  //  book_appoint() {

  //   this.appointmentVal = JSON.stringify(this.appointForm.value);

  //   this.jsonBody = JSON.parse(this.appointmentVal);

  //   console.log("THIS IS THE Appoint raw values VALUES" + this.appointmentVal)
  //   console.log("THIS IS THE Appoint VALUES " + this.jsonBody)

  //   console.log("THIS IS THE Provider ID " + this.from_hosp)
  //   console.log("THIS IS THE REQUESTER ID" + this.requester_id)
  //   console.log("THIS IS THE PROPOSED TIME" + this.jsonBody.proposed_time)
  //    console.log("THIS IS SUBUR ID" + this.sub_id)
  //     console.log("THIS IS THE ALERGIES" + this.jsonBody.allergies)


  //   this.params = {
  //     "suburb_id": this.sub_id,
  //     "provider_id": this.from_hosp,
  //     "requester_id": this.requester_id,
  //     "requester_cat": this.jsonBody.requester_cat,
  //     "beneficiary_name": this.jsonBody.beneficiary_name,
  //     "req_urgency": this.jsonBody.req_urgency,
  //     "appointment_type_id": this.jsonBody.appointment_type_id,
  //     "proposed_date": this.jsonBody.proposed_date + " " + this.jsonBody.proposed_time,
  //     "complaint_desc": this.jsonBody.complaint_desc,
  //     "prev_medical_history": this.jsonBody.prev_medical_history,
  //     "allergies": this.jsonBody.allergies


  //   }



  //   let loader = this.loadingCtrl.create({
  //     content: "Please wait ..."

  //   });

  //   loader.present();


  //   this.data.appointment(this.params).then((result) => {

  //     console.log("THIS IS THE RESULT" + result);
  //     var jsonBody = result["_body"];
  //     console.log(jsonBody);

  //     jsonBody = JSON.parse(jsonBody);
  //     console.log(jsonBody)


  //     var desc = jsonBody["resp_desc"];
  //     var code = jsonBody["resp_code"];


  //     console.log(desc);
  //     console.log(code);

  //     this.messageList = desc;
  //     this.api_code = code;

  //     loader.dismiss();

  //     if (this.api_code == "000") {
  //       let alert = this.alertCtrl.create({
  //         title: "",
  //         subTitle: this.messageList,
  //         buttons: ['OK']
  //       });
  //       alert.present();
  //     }
    
  //   this.navCtrl.setRoot(MenuPage, { value: this.from_login,doc_value: this.from_login3,pers_value: this.from_login2 });
  //     //this.navCtrl.popToRoot()

  //   }, (err) => {
  //     loader.dismiss();
  //     this.toastCtrl.create({
  //       message: "Please check your internet connection",
  //       duration: 5000
  //     }).present();

  //     console.log(err);
  //   });
  // }


}
