import { Component, ViewChild } from '@angular/core';
import { MenuController,NavController, NavParams, ViewController ,App} from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';
import { MenuPage } from '../menu/menu';


@Component({
  selector: 'page-prescription',
  templateUrl: 'prescription.html',
})
export class PrescriptionPage {

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  params2: any = [];
  from_login: any = [];
  from_login_doc: any = [];
  from_login_pers: any = [];
  sub_id: any;
  string: any;
  medication: any;
  med_history: any;
  med_duration: any;
  email: any;
  requester_id: any;

   constructor(public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController,public modalCtrl: ModalController, public viewCtrl: ViewController) {
 
this.from_login = this.navParams.get('value')
 this.from_login_doc = this.navParams.get('doc_value')
  this.from_login_pers = this.navParams.get('pers_value')

 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneConsultPage');
  }






  submit() {

    this.from_login = this.navParams.get('value')
    console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);
    this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
    this.requester_id = this.jsonBody[0].id
    this.check = this.jsonBody[0]

    console.log("THIS IS THE REquester ID " + this.requester_id)
   
 this.params2 = {
      
        "requester_id": this.requester_id,
       "appointment_type_id": "DP",
       "medication": this.medication,
      "duration": this.med_duration,

    
    }

     console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2) )

   console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2) )


      let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    this.data.prescription(this.params2).then((result) => {

      console.log("THIS IS THE RESULT" + result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)


      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

  

      if (this.api_code == "000") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: "Precriptions requested  Succesfully. You would be contacted shortly. Thank you!",
          buttons: ['OK']
        });
        
        this.navCtrl.setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });
        alert.present();
        
      }

      if (this.api_code == "555") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: "Precriptions requested  Succesfully. You would be contacted shortly. Thank you!",
          buttons: ['OK']
        });
        
        this.navCtrl.setRoot(MenuPage, {    value: this.from_login,doc_value: this.from_login_doc,pers_value: this.from_login_pers });
        alert.present();
      }


      // if (this.api_code == "555") {
      //   let alert = this.alertCtrl.create({
      //     title: '',
      //     subTitle: this.messageList,
      //     buttons: ['OK']
      //   });

      
      //   alert.present();
      // }


    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });

  }




  closeModal() {


    this.viewCtrl.dismiss();
  }

 


}


