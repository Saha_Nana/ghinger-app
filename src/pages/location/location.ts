
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { HospitalListPage } from '../hospital-list/hospital-list';
import { HospitalList1Page } from '../hospital-list1/hospital-list1';
import { BookDoctorPage } from '../../pages/book-doctor/book-doctor';
import { PersonalWelPage } from '../../pages/personal-wel/personal-wel';
import { CompleteTestService } from '../../providers/complete-test-service/complete-test-service';
import { Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import 'rxjs/add/operator/map';
import { MenuPage } from '../menu/menu';



@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  @ViewChild('searchbar') myInput;
  @ViewChild('input')
  searchbar: any;

  messageList: any;
  api_code: any;
  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  jsonBody: any;
  params: any = [];
  params2: any = [];
  from_login: any = [];
  from_login2: any = [];
  from_login3: any = [];
  sub_id: any;
  string: any;
  yesorno: any;
  doc_name: any;
  prev_medical_history: any;
  requester_id: any;


  constructor(public toastCtrl: ToastController, private keyboard: Keyboard, public navCtrl: NavController, public navParams: NavParams, public completeTestService: CompleteTestService, public data: DataProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, public viewCtrl: ViewController) {
    this.from_login = this.navParams.get('value')
    this.from_login2 = this.navParams.get('pers_value')
      this.from_login3 = this.navParams.get('doc_value')
   console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS' + this.from_login);
    console.log('LOGIN DETAILS from LOGIN DOC IN MENU PAGE FOR CONSTRUCTOR IS' + this.from_login2);
    console.log('LOGIN DETAILS IN MENU PAGE CONSTRUCTOR IS' + this.from_login3);

//  setTimeout(() => {
//       this.keyboard.show(); // for android
//       this.myInput.setFocus();
//     },1000);




  }

  ionViewDidLoad() {


  }

  closeModal() {


    this.viewCtrl.dismiss();
  }





  go() {




    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    let data = this.searchbar.getValue()
    console.log("LOCATION ENTERED " + data)

    if (data == "") {

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "Please enter a location",
        buttons: ['OK']
      });
      alert.present();
      loader.dismiss();
    }


    else {

      this.params = {

        "location": data

      }
      console.log('PARAMETERS' + this.params);

      this.data.hospitals(this.params).then((result) => {

        console.log("RESULTS IS " + result);
        console.log("RESULTS IS" + this.data.hospitals(this.params));
        var body = result["_body"];
        body = JSON.parse(body);
        this.check = body
        console.log("RESULTS IS " + this.check);
        this.string = JSON.stringify(this.check)
        console.log("LETS SEE THE STRING " + this.string)

        this.jsonBody = JSON.parse(this.string);

        this.sub_id = this.jsonBody[0].suburb_id
        console.log("LETS SEE THE Surburb " + this.sub_id)



        var desc = body["resp_desc"];
        var code = body["resp_code"];


        console.log(desc);
        console.log(code);

        this.messageList = desc;
        this.api_code = code;

      }, (err) => {


        console.log(err);
      });



      console.log("VALUES FROM LOCATION SEARCH" + data);
      console.log(data);
      setTimeout(() => {
        //  this.navCtrl.push(HospitalList1Page,{value: data, another: this.from_login},);
        this.navCtrl.push(BookDoctorPage, { value: data, another: this.from_login, sub_id: this.sub_id,doc_value: this.from_login3,pers_value: this.from_login2 }, );

      }, 3000);

      setTimeout(() => {
        loader.dismiss();
      }, 3000);

    }
  }



  submit() {

    this.from_login = this.navParams.get('value')
    console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);
    this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
    this.requester_id = this.jsonBody[0].id
    this.check = this.jsonBody[0]

    console.log("LETS SEE YESORNO " + this.yesorno)
    console.log("LETS SEE YESORNO " + this.doc_name)
    let data = this.searchbar.getValue()
    console.log("LOCATION ENTERED " + data)
    console.log("THIS IS THE REquester ID " + this.requester_id)
    console.log("THIS IS THE suburb_id ID " + this.sub_id)

    //  let loader = this.loadingCtrl.create({
    //   content: "Please wait ...",
    // });

    // loader.present();


    this.params = {

      "location": data

    }

    console.log('PARAMETERS' + this.params);

    this.data.hospitals(this.params).then((result) => {

      console.log("RESULTS IS " + result);

      var body = result["_body"];
      body = JSON.parse(body);
      this.check = body
      console.log("RESULTS IS " + this.check);
      this.string = JSON.stringify(this.check)
      console.log("LETS SEE THE STRING " + this.string)

      this.jsonBody = JSON.parse(this.string);

      this.sub_id = this.jsonBody[0].suburb_id
      console.log("LETS SEE THE Surburb " + this.sub_id)


      console.log("LETS SEE YESORNO " + this.yesorno)
      console.log("LETS SEE DOC NAME " + this.doc_name)


 this.params2 = {
      "suburb_id": this.sub_id,
      "appointment_type_id": "PD",
      "requester_id": this.requester_id,
      "has_pd": this.yesorno,
      "pd_name": this.doc_name,
      "prev_medical_history": this.prev_medical_history
    }

    console.log("LETS SEE ALL THE PARAMS " + JSON.stringify(this.params2) )


      let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    this.data.doc_appointment(this.params2).then((result) => {

      console.log("THIS IS THE RESULT" + result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)


      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

          if (this.api_code == "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }
      // this.viewCtrl.dismiss();
      //this.navCtrl.setRoot(MenuPage, { 'value': this.from_login,'pers_value': this.from_login2,'doc_value': this.from_login3 });
       this.navCtrl.pop()

      if (this.api_code == "555") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });

      
        alert.present();
      }


    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });

    }, (err) => {

      console.log(err);
    });



    
    }






}
