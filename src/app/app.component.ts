import { Component, ViewChild } from '@angular/core';
import {MenuController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

//import { TabsPage } from '../pages/tabs/tabs';
import { BookMedPage } from '../pages/book-med/book-med';
import { LoginPage } from '../pages/login/login';
 import { HomePage } from '../pages/home/home';
 import { AboutPage } from '../pages/about/about';
  import { MenuPage } from '../pages/menu/menu'; 
//import { MedAppointPage } from '../pages/med-appoint/med-appoint';

@Component({
  templateUrl: 'app.html'

})
export class MyApp {
   @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage; 

  pages: Array<{ title: string, component: any, icon: string }>;
  pages2: Array<{ title: string, component: any, icon: string }>;
  pages3: Array<{ title: string, component: any, icon: string }>;
  pages4: Array<{ title: string, component: any, icon: string }>;
 

  constructor(public menu: MenuController,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public keyboard: Keyboard) {

  this.menu.enable(true);
  //  this.menu.open();
   this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Settings', component: HomePage, icon: "md-settings" },
      { title: 'Help', component: HomePage, icon: "md-help-circle" },
       { title: 'Sign Out', component: AboutPage, icon: "md-power" },
   
      

    ];
  }



  initializeApp() {
    this.platform.ready().then(() => {
      //  this.menu.open();
      // this.menu.enable(true);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


 this.keyboard.disableScroll(false);
      

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ionViewDidEnter() {
    //  this.menu.enable(true);
this.platform.ready().then(() => {
this.keyboard.disableScroll(false);
});
}

ionViewWillLeave() {
  //  this.menu.enable(true);
this.platform.ready().then(() => {
this.keyboard.disableScroll(false); 
});
}

// ionViewDidLoad() {
//     this.menu.enable(true);
//   }


//   ionViewWillEnter(){
//      this.menu.enable(true);
//   }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}
