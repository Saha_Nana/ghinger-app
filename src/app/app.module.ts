import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { MedAppointPage } from '../pages/med-appoint/med-appoint';
import { BookMedPage } from '../pages/book-med/book-med';
import { BookLabPage } from '../pages/book-lab/book-lab';
import { BookMediPage } from '../pages/book-medi/book-medi';
import { BookDoctorPage } from '../pages/book-doctor/book-doctor';
import { HospitalListPage } from '../pages/hospital-list/hospital-list';
import { HospitalList1Page } from '../pages/hospital-list1/hospital-list1';
import { LabHistoryPage } from '../pages/lab-history/lab-history';
import { DocHistoryPage } from '../pages/doc-history/doc-history';
import { PersonalModalPage } from '../pages/personal-modal/personal-modal';
import { PersonalWelPage } from '../pages/personal-wel/personal-wel';
import { DoctorPage } from '../pages/doctor/doctor';
import { MenuPage } from '../pages/menu/menu';
import { SearchPage } from '../pages/search/search';
import { LabSearchPage } from '../pages/lab-search/lab-search';
import { AddRegisPage } from '../pages/add-regis/add-regis';
import { DocpagePersonalPage } from '../pages/docpage-personal/docpage-personal';
import { LocationPage } from '../pages/location/location';
import { NoticePage } from '../pages/notice/notice';
import { PhoneConsultPage } from '../pages/phone-consult/phone-consult';
import { VideoConsultPage } from '../pages/video-consult/video-consult';
import { VidConsultPage } from '../pages/vid-consult/vid-consult';
import { AcceptedAppPage } from '../pages/accepted-app/accepted-app';
import { AcceptedApp2Page } from '../pages/accepted-app2/accepted-app2';
import { PhoneConsListPage } from '../pages/phone-cons-list/phone-cons-list';
import { VideoConsListPage } from '../pages/video-cons-list/video-cons-list';
import { HomeConsListPage } from '../pages/home-cons-list/home-cons-list';
import { PrescriptionConsListPage } from '../pages/prescription-cons-list/prescription-cons-list';
import { PatientNewRecordPage } from '../pages/patient-new-record/patient-new-record';
import { HomeSearchPage } from '../pages/home-search/home-search';
import { PasswordPage } from '../pages/password/password';
import { PrescriptionPage } from '../pages/prescription/prescription';
import { BookHomePage } from '../pages/book-home/book-home';
import { RecordPage } from '../pages/record/record';
import { PatientDetailsPage } from '../pages/patient-details/patient-details';
import { PersonalHomeCarePage } from '../pages/personal-home-care/personal-home-care';
import { MedicationSearchPage } from '../pages/medication-search/medication-search';
import { ReferPage } from '../pages/refer/refer';
import { StatusBar } from '@ionic-native/status-bar'; 
import { SplashScreen } from '@ionic-native/splash-screen';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Keyboard } from '@ionic-native/keyboard';
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { CompleteTestService } from '../providers/complete-test-service/complete-test-service';
import { DataProvider } from '../providers/data/data';
import {RemovehtmltagsPipe} from '../pipes/removehtmltags/removehtmltags';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SignupPage,
    LoginPage,
    MedAppointPage,
    MenuPage,
    HospitalListPage,
    HospitalList1Page,
    BookMedPage,
    SearchPage,
    LabHistoryPage,
    LabSearchPage,
    BookLabPage,
    DocHistoryPage,
    LocationPage,
    BookDoctorPage,
    PersonalModalPage,
    PersonalWelPage,
    DoctorPage,
    PhoneConsultPage,
    AcceptedAppPage,
    NoticePage,
    DocpagePersonalPage,
    AddRegisPage,
    PhoneConsListPage,
    VideoConsultPage,
     VidConsultPage,
    PersonalHomeCarePage,
    VideoConsListPage,
    HomeConsListPage,
    PasswordPage,
    HomeSearchPage,
    BookHomePage,
    PrescriptionPage,
    PatientDetailsPage,
    PatientNewRecordPage,
    AcceptedApp2Page,
    MedicationSearchPage,
    BookMediPage,
    PrescriptionConsListPage,
    RemovehtmltagsPipe,
    RecordPage,
    ReferPage
    
  ],
  imports: [
 
    BrowserModule,
    AutoCompleteModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {

    scrollAssist: false,
    autoFocusAssist:false
  }),
  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SignupPage,
    LoginPage,
    MedAppointPage,
    MenuPage,
    HospitalListPage,
    HospitalList1Page,
    BookMedPage,
    SearchPage,
    LabHistoryPage,
    LabSearchPage,
    BookLabPage,
    DocHistoryPage,
    LocationPage,
    BookDoctorPage,
    PersonalModalPage,
    PersonalWelPage,
    DoctorPage,
    PhoneConsultPage,
    AcceptedAppPage,
    NoticePage,
    DocpagePersonalPage,
    AddRegisPage,
    PhoneConsListPage,
    VideoConsultPage,
    PersonalHomeCarePage,
    VideoConsListPage,
    HomeConsListPage,
    PasswordPage,
    VidConsultPage,
    HomeSearchPage,
    BookHomePage,
    PrescriptionPage,
    PatientDetailsPage,
    PatientNewRecordPage,
    AcceptedApp2Page,
    MedicationSearchPage,
    BookMediPage,
    PrescriptionConsListPage,
    RecordPage,
    ReferPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    Transfer,
    Camera,
    FilePath,
    Keyboard,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CompleteTestService,
    DataProvider
    
   
  ]
})
export class AppModule { }
